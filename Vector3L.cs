﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Synchronis.Utilities.Math
{
    class Vector3L
    {
        #region Static Properties
        public static Vector3L zero = new Vector3L(0, 0, 0);
        public static Vector3L one = new Vector3L(1, 1, 1);
        public static Vector3L forward = new Vector3L(0, 0, 1);
        public static Vector3L back = new Vector3L(0, 0, -1);
        public static Vector3L up = new Vector3L(0, 1, 0);
        public static Vector3L down = new Vector3L(0, -1, 0);
        public static Vector3L right = new Vector3L(1, 0, 0);
        public static Vector3L left = new Vector3L(-1, 0, 0);
        #endregion

        public long x { get; set; }
        public long y { get; set; }
        public long z { get; set; }
        public long magnitude
        {
            get
            {
                return (long)System.Math.Sqrt(x * x + y * y + z * z);
            }
        }
        public long sqrMagnitude
        {
            get
            {
                return x * x + y * y + z * z;
            }
        }
        public Vector3L normalized
        {
            get
            {
                long m = magnitude;

                return new Vector3L(x / m, y / m, z / m);
            }
        }

        public Vector3L()
        {
            Initialize(0, 0, 0);
        }
        public Vector3L(long _x, long _y, long _z)
        {
            Initialize(_x, _y, _z);
        }
        public Vector3L(Vector3L _copy)
        {
            Initialize(_copy.x, _copy.y, _copy.z);
        }
        void Initialize(long _x, long _y, long _z)
        {
            x = _x;
            y = _y;
            z = _z;
        }

        //Dot
        //Cross
        public void Normalize()
        {
            Vector3L n = normalized;

            x = n.x;
            y = n.y;
            z = n.z;
        }

        public override string ToString()
        {
            return "(" + x + ", " + y + ", " + z + ")";
        }
        public override bool Equals(object _obj)
        {
            return base.Equals(_obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #region Static Methods
        public static Vector3L operator +(Vector3L _a, Vector3L _b)
        {
            return new Vector3L(_a.x + _b.x, _a.y + _b.y, _a.z + _b.z);
        }
        public static Vector3L operator -(Vector3L _a, Vector3L _b)
        {
            return new Vector3L(_a.x - _b.x, _a.y - _b.y, _a.z - _b.z);
        }
        public static Vector3L operator *(Vector3L _a, long _b)
        {
            return new Vector3L(_a.x * _b, _a.y * _b, _a.z * _b);
        }
        public static Vector3L operator *(long _a, Vector3L _b)
        {
            return _b * _a;
        }
        public static Vector3L operator /(Vector3L _a, long _b)
        {
            return new Vector3L(_a.x / _b, _a.y / _b, _a.z / _b);
        }
        public static bool operator ==(Vector3L _a, Vector3L _b)
        {
            return _a.x.Equals(_b.x) && _a.y.Equals(_b.y) && _a.z.Equals(_b.z);
        }
        public static bool operator !=(Vector3L _a, Vector3L _b)
        {
            return !_a.x.Equals(_b.x) || !_a.y.Equals(_b.y) || !_a.z.Equals(_b.z);
        }

        public static double Angle(Vector3L _a, Vector3L _b)
        {
            throw new NotImplementedException();
        }
        public static Vector3L Cross(Vector3L _a, Vector3L _b)
        {
            Vector3L cross = new Vector3L();

            cross.x = _a.y * _b.z - _a.z * _b.y;
            cross.y = _a.z * _b.x - _a.x * _b.z;
            cross.z = _a.x * _b.y - _a.y * _b.x;

            return cross;
        }
        public static double Distance(Vector3L _a, Vector3L _b)
        {
            return (_a - _b).magnitude;
        }
        public static double Dot(Vector3L _a, Vector3L _b)
        {
            throw new NotImplementedException();
        }
        public static Vector3L Lerp(Vector3L _a, Vector3L _b, double _amount)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
