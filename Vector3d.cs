﻿using System;

namespace Synchronis.Utilities.Math
{
    public class Vector3d
    {
        #region Static Properties
        public static Vector3d zero = new Vector3d(0, 0, 0);
        public static Vector3d one = new Vector3d(1, 1, 1);
        public static Vector3d forward = new Vector3d(0, 0, 1);
        public static Vector3d back = new Vector3d(0, 0, -1);
        public static Vector3d up = new Vector3d(0, 1, 0);
        public static Vector3d down = new Vector3d(0, -1, 0);
        public static Vector3d right = new Vector3d(1, 0, 0);
        public static Vector3d left = new Vector3d(-1, 0, 0);
        #endregion

        public double x { get; set; }
        public double y { get; set; }
        public double z { get; set; }
        public double magnitude
        {
            get
            {
                return System.Math.Sqrt(x * x + y * y + z * z);
            }
        }
        public double sqrMagnitude
        {
            get
            {
                return x * x + y * y + z * z;
            }
        }
        public Vector3d normalized
        {
            get
            {
                double m = magnitude;

                return new Vector3d(x / m, y / m, z / m);
            }
        }

        public Vector3d()
        {
            Initialize(0, 0, 0);
        }
        public Vector3d(double _x, double _y, double _z)
        {
            Initialize(_x, _y, _z);
        }
        public Vector3d(Vector3d _copy)
        {
            Initialize(_copy.x, _copy.y, _copy.z);
        }
        void Initialize(double _x, double _y, double _z)
        {
            x = _x;
            y = _y;
            z = _z;
        }

        //Dot
        //Cross
        public void Normalize()
        {
            Vector3d n = normalized;

            x = n.x;
            y = n.y;
            z = n.z;
        }

        public override string ToString()
        {
            return "(" + x + ", " + y + ", " + z + ")";
        }
        public override bool Equals(object _obj)
        {
            return base.Equals(_obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #region Static Methods
        public static Vector3d operator +(Vector3d _a, Vector3d _b)
        {
            return new Vector3d(_a.x + _b.x, _a.y + _b.y, _a.z + _b.z);
        }
        public static Vector3d operator -(Vector3d _a, Vector3d _b)
        {
            return new Vector3d(_a.x - _b.x, _a.y - _b.y, _a.z - _b.z);
        }
        public static Vector3d operator *(Vector3d _a, double _b)
        {
            return new Vector3d(_a.x * _b, _a.y * _b, _a.z * _b);
        }
        public static Vector3d operator *(double _a, Vector3d _b)
        {
            return _b * _a;
        }
        public static Vector3d operator /(Vector3d _a, double _b)
        {
            return new Vector3d(_a.x / _b, _a.y / _b, _a.z / _b);
        }
        public static bool operator ==(Vector3d _a, Vector3d _b)
        {
            return _a.x.Equals(_b.x) && _a.y.Equals(_b.y) && _a.z.Equals(_b.z);
        }
        public static bool operator !=(Vector3d _a, Vector3d _b)
        {
            return !_a.x.Equals(_b.x) || !_a.y.Equals(_b.y) || !_a.z.Equals(_b.z);
        }

        public static double Angle(Vector3d _a, Vector3d _b)
        {
            throw new NotImplementedException();
        }
        public static Vector3d Cross(Vector3d _a, Vector3d _b)
        {
            Vector3d cross = new Vector3d();

            cross.x = _a.y * _b.z - _a.z * _b.y;
            cross.y = _a.z * _b.x - _a.x * _b.z;
            cross.z = _a.x * _b.y - _a.y * _b.x;

            return cross;
        }
        public static double Distance(Vector3d _a, Vector3d _b)
        {
            return (_a - _b).magnitude;
        }
        public static double Dot(Vector3d _a, Vector3d _b)
        {
            throw new NotImplementedException();
        }
        public static Vector3d Lerp(Vector3d _a, Vector3d _b, double _amount)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
